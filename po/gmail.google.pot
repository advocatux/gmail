# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Rudi Timmermans
# This file is distributed under the same license as the Gmail package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Gmail\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-20 07:36+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/QrCodeDialog.qml:11 ../qml/DownloadingDialog.qml:31
#: ../qml/DownloadingDialog.qml:50
msgid "Close"
msgstr ""

#: ../qml/AboutPage.qml:8
msgid "About Gmail v1.8.6"
msgstr ""

#: ../qml/AboutPage.qml:9
msgid "This is a Gmail Webapp for Ubuntu Touch."
msgstr ""

#: ../qml/AboutPage.qml:13
msgid ""
"Brian Douglass: Writer of Downloadinterceptor, whose work made updloading "
"attachments and downloading attachments possible."
msgstr ""

#: ../qml/AboutPage.qml:18
msgid ""
"Copyright (c) 2018 <br> by Rudi Timmermans  <br><br> E-Mail: <a href="
"\"mailto://rudi.timmer@mail.ch\">rudi.timmer@mail.ch</a>"
msgstr ""

#: ../qml/AboutPage.qml:23
msgid "Special thanks to tester, Tomas Öqvist"
msgstr ""

#: ../qml/AboutPage.qml:27
msgid "OK"
msgstr ""

#: ../qml/DownloadingDialog.qml:9
msgid "Download in progress"
msgstr ""

#: ../qml/DownloadingDialog.qml:29
msgid "Cancel"
msgstr ""

#: ../qml/actions/SaveImage.qml:22
msgid "Save image"
msgstr ""

#: ../qml/actions/CopyImage.qml:22
msgid "Copy image"
msgstr ""

#: ../qml/actions/ShareLink.qml:22
msgid "Share…"
msgstr ""

#: ../qml/actions/Copy.qml:22
msgid "Copy"
msgstr ""

#: ../qml/actions/CopyLink.qml:22
msgid "Copy link"
msgstr ""

#: ../qml/SadPage.qml:27
msgid "Oops, something went wrong. Reload from bottom menu."
msgstr ""

#: ../qml/ErrorSheet.qml:18
msgid "Network Error"
msgstr ""

#. TRANSLATORS: %1 refers to the URL of the current page
#: ../qml/ErrorSheet.qml:24
msgid "It appears you are having trouble viewing: %1."
msgstr ""

#: ../qml/ErrorSheet.qml:30
msgid "Please check your network settings and try refreshing the page."
msgstr ""

#: ../qml/ErrorSheet.qml:35
msgid "Refresh page"
msgstr ""

#: ../qml/Main.qml:114
msgid "Copy Link"
msgstr ""

#: ../qml/Main.qml:122
msgid "Share Link"
msgstr ""

#: ../qml/Main.qml:138
msgid "Copy Image"
msgstr ""

#: ../qml/Main.qml:144
msgid "Download Image"
msgstr ""
